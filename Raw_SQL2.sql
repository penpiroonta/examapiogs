จากข้อที่ 
1. เขียน Raw SQL ด้วยภาษา MySQL กรองข้อมูลประวัติจองห้องพัก (ผลลัพธ์ที่ได้ต้องสมเหตุสมผลต่อการใช้งานจริง)
ผลลัพธ์ที่ต้องแสดง
- รายการประวัติจองห้องพักที่มีการจองช่วงเวลาที่กรอง
เงื่อนไขที่ต้องทำ ใช้โค้ดจากข้อที่ 1. รับ Input 2 ค่า (เวลาเริ่มต้น, เวลาสิ้นสุด)
ผลลัพธ์จากข้อที่ 2. โดยมี Input เป็นเวลาเริ่มต้น 2021-09-02 15:00:00 และเวลาสิ้นสุด 2021-09-02 19:00:59
 


Command : 
SELECT
concat(users.name_prefix, " ", users.first_name, "  ", users.last_name) AS  fullname ,
    bookings.start_date as start_date, 
    bookings.end_date as end_date, 
    rooms.room_name  as room_name
FROM exam.bookings
left join exam.users on (bookings.users_id = users.id)
left join exam.rooms on (bookings.rooms_id = rooms.id)
WHERE ('2021-09-02 15:00:00' BETWEEN bookings.start_date AND bookings.end_date) OR
('2021-09-02 19:00:59' BETWEEN bookings.start_date AND bookings.end_date) 
HAVING fullname is not null



