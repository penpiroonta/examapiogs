module examapiogs

go 1.16

require (
	github.com/labstack/echo/v4 v4.10.0
	golang.org/x/crypto v0.5.0 // indirect
)
