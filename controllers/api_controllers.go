package controllers

import (
	"examapiogs/service"

	"github.com/labstack/echo/v4"
)

func ApiControllers(ctx echo.Context) error {

	users := service.GetUserApiOgs()
	return ctx.JSON(200, map[string]interface{}{
		"data": users,
	})

}
