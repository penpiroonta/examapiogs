package models

type Users struct {
	ID     int    `json:"id"`
	Name   string `json:"name"`
	Email  string `json:"email"`
	Gender string `json:"gender"`
	Status string `json:"status"`
}

// type Post struct {
// 	ID     int    `json:"id"`
// 	UserID int    `json:"user_id"`
// 	Title  string `json:"title"`
// 	Body   string `json:"body"`
// }
