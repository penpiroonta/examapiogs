package service

import (
	"encoding/json"
	"examapiogs/models"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func GetUserApiOgs() []models.Users {

	// ! ตั้งค่า
	url := "https://gorest.co.in/public/v2/users"
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Print(err.Error())
	}

	// ! ยิง api
	// req.Header.Add("x-rapidapi-key", "YOU_API_KEY")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Print(err.Error())
	}

	// ! ปิด connection
	defer res.Body.Close()

	// ! อ่าน response body
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		fmt.Print(err.Error())
	}
	// fmt.Println(string(body))

	// ! ยัดใส่ตัวแปร user
	users := []models.Users{}

	err2 := json.Unmarshal(body, &users)

	if err2 != nil {
		log.Fatal(err2)
	}

	user2 := []models.Users{}
	for _, user := range users {
		if user.Status == "active" {
			user2 = append(user2, user)
		}
	}

	return user2

}
