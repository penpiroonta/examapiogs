package main

import (
	"examapiogs/controllers"

	"github.com/labstack/echo/v4"
)

func main() {

	e := echo.New()

	e.GET("/users", controllers.ApiControllers)

	e.Logger.Fatal(e.Start(":1323"))

}
