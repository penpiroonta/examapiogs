เขียน Raw SQL ด้วยภาษา MySQL แสดงรายการประวัติจองห้องพักทั้งหมด ผลลัพธ์ที่ได้ต้องสมเหตุสมผลต่อการใช้งานจริง
ฟิลด์ที่ต้องแสดง
- ชื่อเต็ม(คำนำหน้า, ชื่อ, นามสกุล)
- เวลาเริ่มจอง
- เวลาสิ้นสุดการจอง
- ชื่อห้อง
- เงื่อนไขที่ต้องทำ ใช้ Join



Command : 
SELECT
concat(users.name_prefix, " ", users.first_name, "  ", users.last_name) AS  fullname ,
    bookings.start_date as start_date, 
    bookings.end_date as end_date, 
    rooms.room_name  as room_name
FROM exam.bookings
left join exam.users on (bookings.users_id = users.id)
left join exam.rooms on (bookings.rooms_id = rooms.id)
HAVING fullname is not null
order by  bookings.start_date ASC ; 


FROM exam.bookings
left join exam.users on (bookings.users_id = users_id)
left join exam.rooms on (bookings.rooms_id = rooms_id)